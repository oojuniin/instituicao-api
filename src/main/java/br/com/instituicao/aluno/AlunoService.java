package br.com.instituicao.aluno;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AlunoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private AlunoRepository repository;

	@Transactional(readOnly = true)
	public List<AlunoDTO> findAll() {
		List<Aluno> alunos = repository.findAll();
		return alunos.stream().map(aluno -> new AlunoDTO(aluno)).collect(Collectors.toList());
	}

	@Transactional
	public AlunoDTO save(AlunoDTO dto) {
		Aluno aluno = new Aluno(dto.getNome(), dto.getSobrenome(), dto.getMatricula(), dto.getCurso());
		aluno = repository.save(aluno);
		return new AlunoDTO(aluno);
	}
}
