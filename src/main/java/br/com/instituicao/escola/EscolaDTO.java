package br.com.instituicao.escola;

import java.io.Serializable;

import org.modelmapper.ModelMapper;

import br.com.instituicao.util.Endereco;

public class EscolaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String nome;
	private Endereco endereco;

	public EscolaDTO() {
	}

	public EscolaDTO(long id, String nome, Endereco endereco) {
		this.id = id;
		this.nome = nome;
		this.endereco = endereco;
	}

	public static EscolaDTO create(Escola escola) {
		ModelMapper model = new ModelMapper();
		return model.map(escola, EscolaDTO.class);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EscolaDTO other = (EscolaDTO) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
